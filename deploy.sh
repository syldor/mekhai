#!/bin/bash
set -e

# Run this in your Hugo blog repo directory

BUCKET_NAME=mekham-delivery.la
PROFILE=default # or `default` if you don't use profiles

npm run build

# Copy over pages - not static js/img/css/downloads
aws s3 sync --profile ${PROFILE} --acl "public-read" --sse "AES256" dist/ s3://${BUCKET_NAME}/ 

# Ensure static files are set to cache forever - cache for a month --cache-control "max-age=2592000"
aws s3 sync --profile ${PROFILE} --cache-control "max-age=2592000" --acl "public-read" --sse "AES256" dist/img/ s3://${BUCKET_NAME}/img/ 
aws s3 sync --profile ${PROFILE} --cache-control "max-age=2592000" --acl "public-read" --sse "AES256" dist/css/ s3://${BUCKET_NAME}/css/ 
aws s3 sync --profile ${PROFILE} --cache-control "max-age=2592000" --acl "public-read" --sse "AES256" dist/js/ s3://${BUCKET_NAME}/js/ 


