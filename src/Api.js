import axios from 'axios'

export default () => {
  return axios.create({
    baseURL: 'https://script.google.com/macros/s/AKfycbyGusyVD01V4CEfrLhkOWCOPGDgEYkH_9oVN-kfuEAjCfxSjf4/'
  })
}
